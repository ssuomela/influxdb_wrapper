# Wrapper for InfluxDB to validate input with Pydantic 

This is a small package that adds input validation to influxdb python
connector (https://github.com/influxdata/influxdb-python).

The input validation is done with pydantic
(https://pydantic-docs.helpmanual.io/).   

I created this package for playing with the influxdb data collected by bluetooth sensors.

## Running tests

Tests are run using tox, which creates virtual environment with the required
packages. In order to run tests, tox must be preinstalled, i.e., ```pip install tox```

You can then run the test in the influxdb-wrapper folder with
```bash
tox
```

## How-to-use

### Initialization
You can initialize the wrapper in a similar manner than influxDBClient
with the exception field_definitions must be given:
```python
from influxdb_wrapper.wrapper import InfluxDBWrapper

wrapper = InfluxDBWrapper(
    field_definitions=field_definitions,
    *influxdb_client_args,
    **influxdb_client_kwargs
)
```
where `influxdb_client_args` and `influxdb_client_kwargs` would be the args and
kwargs that you would use to connect to influxDB without the wrapper. 

If you are running InfluxDB in localhost with default port, the wrapper could
be for example something like:
initialized with:
```python
from influxdb_wrapper.wrapper import InfluxDBWrapper

field_definitions = {
    "int_field": int,
    "int_field_with_default_value": (int, 1),
    "string_field_with_default": (str, "my_default"),
    "float_field": 5.0,
    "bool_field": True,
}
wrapper = InfluxDBWrapper(
    field_definitions=field_definitions, host="localhost", port=8086
)
```
Field definitions must be a dictionary whose key gives the name of the field
and the value gives the type. The value must be either a tuple where the first
element gives the field type and the second one the default value. If you don't
want to give default value, then use Ellipsis, i.e. ... as the default value in the tuple.

### Write points
After creating a wrapper instance, you can write to influxdb using the same
arguments as with InfluxDbClient, e.g.:
```python
points = [
    {
        "measurement": "Example",
        "tags": {
            "id": "sample_id",
        },
        "time": "2022-07-23T8:00:00Z",
        "fields": {
            "int_field": "127",
            "string_field_default": "something",
            "float_field": 5,
        },
    },
]
wrapper.write_points(points=points, *args, **kwargs)
```
The wrapper will modify the fields to the correct type before writing to
influxDB. As "int_field_with_default_value" was not in the fields, it will be
added with its default value.

Wrapper writing supports also an optional input parameter fraction, which
allows to run the input validation only for the given fraction of the points.
This is mainly meant to be used for larger data sets and especially with strict_types, e.g., StrictInt and
StrictStr. In that situation, the parameter fraction can be used to run the validation
for a fraction of the points and help to debug the source of points that are of wrong type. 

## Issues

* Using pydantic will slow down the writing to influxdb so for writing large
datasets, it's better to do the validation first and write it to influxdb
without pydantic, i.e., this package.


