"""Packaging logic."""
import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="influxdb_client_wrapper",
    version="0.0.1",
    author="Samu Suomela",
    author_email="suomela.samu@gmail.com",
    description=(
        "Wrapper for influxdb connector to validate input with pydantic"
    ),
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="TBA",
    classifiers=[
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=setuptools.find_packages("."),
    python_requires=">=3.7",
    install_requires=["influxdb", "pydantic"],
)
