"""
Module for InfluxDB datatypes and their extensions suitable
for creation of pydantic models.
"""
import logging
from platform import python_version
from typing import Tuple, TypeVar, Union

logger = logging.getLogger(__name__)

try:
    from types import EllipsisType  # type: ignore
except ImportError:
    logger.warning(
        "EllipsisType not found. Python version >= 3.10 required. "
        "Current Python version %s.",
        python_version(),
    )
    logger.warning("Falling back to create EllipsisType with type(Ellipsis)")
    EllipsisType = type(Ellipsis)


# Support datatypes by InfluxDB
# (https://docs.influxdata.com/influxdb/v1.8/write_protocols/line_protocol_tutorial/#data-types)
InfluxData = TypeVar("InfluxData", float, int, bool, str)

influx_supported_datatypes = (
    InfluxData.__constraints__  # type: ignore # pylint: disable=no-member
)

# Pydantic support creation of dynamic models through create_model,
# which takes fields in the form `<name>=(<type>, <default default>)`
# or `<name>=<default value>
# Ellipsis (...) as default means no default is given
InfluxDefaultValue = Union[InfluxData, EllipsisType]

influx_supported_default_values = influx_supported_datatypes + (EllipsisType,)

InfluxFieldType = Union[Tuple[type, InfluxDefaultValue], InfluxData]
