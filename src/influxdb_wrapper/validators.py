"""Module for pydantic type validation"""
import logging
from typing import Any, Dict, Optional

from pydantic import BaseModel, create_model, validator

from .types import (
    InfluxFieldType,
    influx_supported_datatypes,
    influx_supported_default_values,
)

logger = logging.getLogger(__name__)


class InfluxFields(BaseModel):
    """
    Pydantic model that checks that field definitions are given in the form
    supported by create_model.
    Using Any instead of InfluxFieldType due to the bug in pydantic unions.
    See https://github.com/samuelcolvin/pydantic/issues/1723
    """

    # pylint: disable=too-few-public-methods
    fields: Dict[str, Any]

    @validator("fields")
    @classmethod
    def check_fields_length(cls, fields: Dict[str, Any]) -> Dict[str, Any]:
        """Make sure that empty dict is not accepted as valid input"""
        assert len(fields) > 0, "fields should not be empty"
        return fields

    class Config:
        """
        Use default configs except allow arbitrary types,
        e.g., type(Ellipsis).
        """

        arbitrary_types_allowed = True


def create_influx_write_validator(
    field_definitions: Dict[str, InfluxFieldType]
) -> type:
    """
    Create a pydantic model dynamically using the fields
    definitions.
    """
    logger.debug(
        "Creating write_validator with field definitions: %s",
        field_definitions,
    )

    for value in field_definitions.values():
        try:
            if isinstance(value, tuple):
                field_type, field_default = value

                if not isinstance(
                    field_default, influx_supported_default_values
                ):  # noqa: E501
                    raise TypeError
            else:
                field_type = type(value)

            if field_type not in influx_supported_datatypes:
                raise TypeError

        except (ValueError, TypeError) as error:
            raise ValueError(
                "field_definitions' values must be either of the form:"
                "`tuple(<type>, <default value>)` or `<default_value>`"
            ) from error

    class InfluxModel(BaseModel):
        """
        Pydantic Model for writing to InfluxDB with InfluxDB-Python
        connector.
        """

        measurement: str
        fields: create_model("Fields", **field_definitions)  # type: ignore # noqa: E501,F821
        tags: Optional[Dict[str, str]] = None
        time: Optional[str] = None

    return InfluxModel
