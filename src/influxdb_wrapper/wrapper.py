"""Main API of the package"""
from random import random
from typing import Any, Dict, List

from influxdb import InfluxDBClient

from .types import InfluxFieldType
from .validators import InfluxFields, create_influx_write_validator


class InfluxDBWrapper(InfluxDBClient):
    """
    Wrapper class for InfluxDBClient that includes type validation
    for the input by using pydantic.
    """

    __doc__ += InfluxDBClient.__doc__

    def __init__(
        self,
        field_definitions: Dict[str, InfluxFieldType],
        *args: Any,
        **kwargs: Any
    ) -> None:
        super().__init__(*args, **kwargs)
        self.write_validator = field_definitions  # type: ignore

    @property
    def write_validator(self) -> type:
        """Pydantic Model used for validating data before writing to
        InfluxDB"""
        return self._write_validator

    @write_validator.setter
    def write_validator(
        self, field_definitions: Dict[str, InfluxFieldType]
    ) -> None:
        """
        Setup write validator from field_definitons by first
        validating the field definitions.
        """
        influx_fields = InfluxFields(fields=field_definitions)
        self._write_validator = create_influx_write_validator(
            field_definitions=influx_fields.fields
        )

    def validate_point(self, point: dict) -> dict:
        """Validate individual point using the write validator."""
        return self._write_validator(**point).dict()

    def write_points(
        self,
        points: List[dict],
        *args: Any,
        fraction: float = 1.0,
        **kwargs: Any
    ) -> bool:
        """
        Write points to influxDB after validating fraction of them.

        Args:
            fraction: Fraction of points that are run through the validation
        Returns:
            True if success, False otherwise.
        """
        points = [
            self.validate_point(point) if random() < fraction else point
            for point in points
        ]
        return super().write_points(points, *args, **kwargs)
