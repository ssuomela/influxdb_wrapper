"""Pytests conftest file for test fixtures shared by all the tests."""
from typing import Iterator

import pytest


@pytest.fixture(scope="session")
def initialization_fields() -> Iterator:
    "Fields given to wrapper for tests." ""
    fields = {"Int_field": (int, 1), "Int_field2": -1, "String_field": "1"}
    yield fields
