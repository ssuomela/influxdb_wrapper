"""Test wrapper"""
from typing import Iterator
from unittest import mock

import pytest

from influxdb_wrapper.wrapper import InfluxDBWrapper


@pytest.fixture(name="data_fields")
def fixture_data_fields() -> Iterator:
    """Data used for tests"""
    fields = {"Int_field": "2", "Int_field2": -2.0, "String_field": 1}
    yield fields


@pytest.fixture(name="expected_data_fields")
def fixture_expected_data_fields() -> Iterator:
    """Expect output after validation."""
    fields = {
        "Int_field": 2,
        "Int_field2": -2,
        "String_field": "1",
    }
    yield fields


@pytest.fixture(name="wrapper")
def fixture_wrapper(initialization_fields: dict) -> Iterator[InfluxDBWrapper]:
    """Initialize Wrapper with the given field types and defaults without
    initializing influxdb"""
    with mock.patch("influxdb.InfluxDBClient.__init__"):
        with mock.patch("influxdb.InfluxDBClient.write_points"):
            yield InfluxDBWrapper(initialization_fields)


def test_write_points(wrapper: InfluxDBWrapper, data_fields: dict) -> None:
    """Test write_points function"""

    data = {
        "measurement": "test",
        "fields": data_fields,
        "tags": {"host": "local", "region": "eu-west"},
        "time": "2022-05-15T08:00:00Z",
    }

    assert wrapper.write_points([data])


def test_write_validator_setter() -> None:
    """Test validator setter"""
    with mock.patch("influxdb.InfluxDBClient.__init__"):
        # These should all be invalid:
        with pytest.raises(Exception):
            InfluxDBWrapper([])  # type: ignore

        with pytest.raises(Exception):
            InfluxDBWrapper({})  # type: ignore

        # This test should fail but due to a loose instance type checking in
        # github.com/samuelcolvin/pydantic/blob/master/pydantic/main.py#L968
        # it doesn't fail.
        # with pytest.raises(Exception):
        #     InfluxDBWrapper({"InvalidOrder": (..., str)})  # type: ignore

        with pytest.raises(Exception):
            InfluxDBWrapper({"InvalidSyntax": int})  # type: ignore

        # These should all be valid:
        InfluxDBWrapper({"ValidSample": (str, ...)})

        InfluxDBWrapper({"ValidSample": (int, ...)})

        InfluxDBWrapper({"ValidSample": 5})

        InfluxDBWrapper({"ValidSample": "Test"})

        InfluxDBWrapper({"ValidSample": True})

        InfluxDBWrapper({"ValidSample": False})

        InfluxDBWrapper({"ValidSample": -2.0})

        # This will pass as pydantic parses the key to be string
        InfluxDBWrapper({123: 1})  # type: ignore


def test_validate_point(
    wrapper: InfluxDBWrapper, data_fields: dict, expected_data_fields: dict
) -> None:
    """Test write validator with data"""
    data = {
        "measurement": "test",
        "fields": data_fields,
        "tags": {"host": "local", "region": "eu-west"},
        "time": "2022-05-15T08:00:00Z",
    }

    expected_data = {
        "measurement": "test",
        "fields": expected_data_fields,
        "tags": {"host": "local", "region": "eu-west"},
        "time": "2022-05-15T08:00:00Z",
    }

    validated_data = wrapper.validate_point(data)
    assert validated_data == expected_data
