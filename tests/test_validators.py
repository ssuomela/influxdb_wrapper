"""Test types"""
from typing import Iterator

import pytest
from pydantic import BaseModel

from influxdb_wrapper.validators import create_influx_write_validator


@pytest.fixture(name="expected_field_schema")
def fixture_expexted_field_schema() -> Iterator:
    """Expected Influxdb field types"""
    fields = {
        "Int_field": "integer",
        "Int_field2": "integer",
        "String_field": "string",
    }
    yield fields


def test_create_influx_write_validator(
    initialization_fields: dict, expected_field_schema: dict
) -> None:
    """Test write validator"""
    model = create_influx_write_validator(initialization_fields)

    assert issubclass(model, BaseModel)

    schema = model.schema()

    properties = schema["properties"].keys()
    assert "measurement" in properties
    assert "fields" in properties
    assert "tags" in properties
    assert "time" in properties

    # Check types
    assert schema["properties"]["time"]["type"] == "string"
    assert schema["properties"]["measurement"]["type"] == "string"
    assert schema["properties"]["tags"]["type"] == "object"
    assert (
        schema["properties"]["tags"]["additionalProperties"]["type"] == "string"
    )

    # Check fields attribute
    schema_fields = schema["definitions"]["Fields"]["properties"]
    schema_field_types = {
        key: val["type"] for key, val in schema_fields.items()
    }

    assert schema_field_types == expected_field_schema
